# Noter BE - Compiled spring boot REST backend (java 11) for noter app

### Jar file has to be in the same folder as notedb.sqlite file !
### Deployment is just through starting jar file (java 11 required)

```
java -jar <file>.jar
```

### OR just start attached docker-compose file

```
docker-compose up -d
```


### Service runs by default on port 8080

### Sqlite DB user table contains by default user with

````
username:admin

password:admin
````


## Source code of noter BE is located here:

https://bitbucket.org/sebesta_jozef/noter-be/src/master/

## FE (react app) is located here:

https://bitbucket.org/sebesta_jozef/noter/src/master/

